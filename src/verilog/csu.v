module CSU #(
	parameter bits 	= 12
)(
	input 	[bits-1:0] data_in,
	input 	clk,
	output 	reg [bits+1:0] data_out
);

	localparam pipes = 4;		// number of pipeline stages
	
	reg signed [bits-1:0] pipeline [0:pipes-1];
	reg signed [bits-1:0] trans, staircase;
	reg signed [1:0] direction;
	integer i;
	
	always @(posedge clk) begin
		pipeline[0] <= data_in;
		for (i = 1; i < pipes; i=i+1) begin
			pipeline[i] <= pipeline[i-1];
		end
		
		trans <= pipeline[1] - pipeline[0];
		
		if (trans < -'d243497802) 
			direction <= 2'b11;
		else if (trans > 'd243497802) 
			direction <= 2'b01;
		else 
			direction <= 2'b00;
			
		staircase 	<= staircase + direction;
		data_out 	<= staircase * 'd536870912 + pipeline[3];
	end
endmodule