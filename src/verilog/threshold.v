// pipelined thresholding module
// inputs a complex number and allows it to pass 
// if it's magnitude is big enough

module Threshold #(
	parameter bits = 12
)(
	input 		[bits-1:0] iRe, iIm,
	input		clk,
	output reg	[bits-1:0] oRe, oIm
);

	reg signed [bits-1:0] iRe_pipe[0:2];
	reg signed [bits-1:0] iIm_pipe[0:2];
	reg signed [2*bits-1:0] product[0:1];
	reg signed [bits-1:0] sum;
	integer i;
	
	always @(posedge clk) begin 
		iRe_pipe[0] <= iRe;
		iIm_pipe[0] <= iIm;

		for (i=1; i < 3; i=i+1) begin
			iRe_pipe[i] <= iRe_pipe[i-1];
			iIm_pipe[i] <= iIm_pipe[i-1];
		end
		
		product[0] <= iRe_pipe[0] * iRe_pipe[0];
		product[1] <= iIm_pipe[0] * iIm_pipe[0];
		
		sum <= (product[0] >> bits) + (product[1] >> bits);	// correction for fixed point mul
		
		oRe <= sum < 'd32 ? 'b0 : iRe_pipe[2];
		oIm <= sum < 'd32 ? 'b0 : iIm_pipe[2];
	end

endmodule 
