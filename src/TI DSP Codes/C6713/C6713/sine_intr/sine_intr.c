//sine_intr.c Sine generation using sin() function
#include <math.h>
#include "DSK6713_AIC23.h"	                // codec support
Uint32 fs=DSK6713_AIC23_FREQ_8KHZ;	        //set sampling rate
#define DSK6713_AIC23_INPUT_MIC 0x0015
#define DSK6713_AIC23_INPUT_LINE 0x0011
Uint16 inputsource=DSK6713_AIC23_INPUT_MIC; // select input

#define SAMPLING_FREQ 8000
#define PI 3.14159265358979

float frequency = 1000.0;
float amplitude = 10000.0;
float theta_increment;
float theta = 0.0;

interrupt void c_int11()
{
  theta_increment = 2*PI*frequency/SAMPLING_FREQ;
  theta += theta_increment;
  if (theta > 2*PI) theta -= 2*PI;
  output_left_sample((short)(amplitude*sin(theta)));
  return;
}

void main()
{
  comm_intr();
  while(1);
}
