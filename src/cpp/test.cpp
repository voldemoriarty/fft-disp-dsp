//
// Created by saad on 2/17/19.
//

// https://www.semanticscholar.org/paper/FPGA-based-signal-processing-in-an-optical-feedback-Li-Yua/a50cf52826244f358e2405c7d8374ad018f7e771

#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>

#include "utils.h"

using vec = std::vector<float>;

/*
 * Reads floats from a file
 * floats should be whitespace seperated
 */
vec readFromFile(const char* file) {
    std::ifstream fs(file);
    std::istream_iterator<float> start(fs), end;
    vec v(start, end);
    fs.close();
    return v;
}


/*
 * Writes a list of floats to a file seperated by '\n'
 */
void writeToFile(const char* file, const vec &arr) {
    std::ofstream fs(file);
    std::copy(arr.begin(), arr.end(), std::ostream_iterator<float> (fs, "\n"));
    fs.close();
}

int main() {
    vec sig = readFromFile("../test/data/psim.dat");
    vec phase(sig.size(), 0.0f), staircase(sig.size()), temp(sig.size());
    const int n = phase.size();
    std::cout << "Read " << sig.size() << " values.\n";
    
    phase_unwrap(&sig[0], &phase[0], &staircase[0], sig.size());
//     offset(&phase[0], n, -1.0f * mean(&phase[0], n)); // phase = phase - mean(phase)
    writeToFile("../test/data/phase.dat", phase);
//     writeToFile("../test/data/staircase.dat", staircase);
//     writeToFile("../test/data/tr.dat", sig);

    // phase contains real part of fft
    // staircase contains imaginary part of fft
    zero_out(&staircase[0], staircase.size());
    fft(&phase[0], &staircase[0], n, &sig[0], &temp[0]);
//     writeToFile("../test/data/fft-real.dat", phase);
//     writeToFile("../test/data/fft-imag.dat", staircase);

//     threshold(&phase[0], &staircase[0], &temp[0], n);       // all values except 0.8 of max are zeroed out
//     writeToFile("../test/data/fft-thres-real.dat", phase);
//     writeToFile("../test/data/fft-thres-imag.dat", staircase);

    int maxi = 1;
    float max = 0, ax = phase[1], ay = staircase[1];
    
    for (int i = 1; i < phase.size(); ++i) {
        const float mag_eq = fabsf(phase[i]) + fabsf(staircase[i]);
        if (mag_eq > max) {
            max = mag_eq;
            ax = phase[i];
            ay = staircase[i];
            maxi = i;
        }
        
        phase[i] = 2.0f / (float)phase.size();
    }
    phase[0] = 0.0f;
    
    for (int i = 0; i < phase.size(); ++i) {
        const float N = (float) phase.size();
        const float bx = cosine(2.0f*PI*i*maxi/N);
        const float by = sine(2.0f*PI*i*maxi/N);
        
        phase[i] *= (ax*bx - ay*by);
    }
    
    
    
//     ifft(&phase[0], &staircase[0], n, &sig[0], &temp[0]);
    writeToFile("../test/data/ifft-real.dat", phase);
//     writeToFile("../test/data/ifft-imag.dat", staircase);

    std::cout << "Wrote " << phase.size() << " values.\n";
    return 0;
}
