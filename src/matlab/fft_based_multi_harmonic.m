phi = pi/2;
C = 1.5;
alpha = 5;
amp = 10*0.5;                   % No of Lambda   : 16 gets false detection
K = 5;

fs = 2^16;              % X samples per sec
dt = 1/fs;

time_in_sec = 1;
M = fs*time_in_sec;     % total no of acquisition points

time = (0:dt:time_in_sec)';
time = time(1:end-1);

disp = zeros(M, 1);

freq_band = [20];

for i = freq_band
    disp = disp + sin(2*pi*i*time);
end

disp = disp / length(freq_band) * 2*pi*amp;

xs_sim = K*pi - atan(alpha) + disp + phi;

N = length(xs_sim);

psim = cos(xc_fonction_xs_interpol_f_xs(xs_sim, C, atan(alpha)));

lambda = 0.785;

psim_norm = 2*(psim - min(psim))/(max(psim) - min(psim)) - 1;   % normalize [-1 1]

trans = psim_norm(2:end) - psim_norm(1:end-1);  % compute first forward difference

trans = [trans; trans(end)];                    % equalize the sizes

trans_max = 0.5*max(trans);                    % get the threshold

trans(abs(trans) < trans_max) = 0;              % get the transitions
trans = trans ./ (abs(trans) + 1e-50);          % add a very small value to avoid NaNs

staircase = cumsum(trans * 2 * pi);             % each trans corresponds to 2*pi phase

phase = -staircase - acos(psim_norm);           % add acos to get phase unwrapped

phase_f = fft(phase);
dc = phase_f(1);
phase_f(1) = 0;                                             % remove dc, offset

phase_mag_eq = abs(real(phase_f)) + abs(imag(phase_f));     % gate saving jugaar; saves 2 multipliers
phase_mag = abs(phase_f);

[phase_f_max, i] = max(phase_mag_eq);
phase_f(phase_mag_eq < phase_f_max * 0.1) = 0;

% for one pt/hz, i is the frequency we need
% but we still need the computation for phase

% rec = ifft(phase_f, 'symmetric');

rec = zeros(M, 1);
tones = find(phase_f(1:end/2));      % find non zero elements
                                     % end/2 because symmetry

k = 0:fs-1;
                                     
for i = tones'
    Xmx = phase_f(i);               % symmetric pair
    ex = exp(pi*2j*(i-1)*k / fs);
    
    ax = real(Xmx);
    ay = imag(Xmx);
    bx = real(ex);
    by = imag(ex);
    
    rec = rec + 2/fs * (ax*bx - ay*by)';        % check notes for math
end


Displ_in_nm = (rec ./(2*pi)).*(lambda/2) * 1000;
disp_in_nm = (disp ./(2*pi)).*(lambda/2) * 1000;

subplot(2, 1, 1);
plot(0:1/fs:(1-1/fs), Displ_in_nm);
hold on;
title('Displacement (nm)');
plot(0:1/fs:(1-1/fs), disp_in_nm);
legend('Recovered', 'Original');

subplot(2, 1, 2);
plot(0:1/fs:(1-1/fs), disp_in_nm - Displ_in_nm);
title('Rec - Orig (nm)');
