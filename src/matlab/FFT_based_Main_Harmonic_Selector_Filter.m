%% this function has time domain input and output, u give it the input
%% matrix  and its sampling_f and then the code returns just a signal based on the main harmonic of the input signal 

function [MH_signal] = FFT_based_Main_Harmonic_Selector_Filter (input_signal,sampling_f)



N = length(input_signal);

%dt = time(2,1) - time(1,1);
%sampling_f = 1/dt;
f=[0:1:N/2-1]*sampling_f/N;
f=f';



fft_input = fft(input_signal);

% Derivative of FFT 's abs values
fft_input_derivative = zeros(length(fft_input),1);
fft_input_abs = abs(fft_input);
fft_input_derivative(1:end-1) = fft_input_abs(2:end) - fft_input_abs(1:end-1); % note the flipped derivative to cater for the decaying shape of FFT spectrum

use_of_derivative_ON =1
if use_of_derivative_ON == 1
    fft_matrix_for_max_location = fft_input_derivative;
else
    fft_matrix_for_max_location = fft_input;
end

% [max_value ,max_ind] = max(fft_matrix_for_max_location(2:end/2,1))  % Select max from everything except the DC value  i.e. the 1st sample on fft spectrum
%                                              % Note that end/2 means only
%                                              % the first side from the
%                                              % FFT mirror has been chosen
% 
% [ max_value2, max_ind2] = max (fft_matrix_for_max_location((end/2)+1:end)) % the other mirror 
%                

[max_value ,max_ind] = max(fft_matrix_for_max_location(2:end/2,1))  % Select max from everything except the DC value  i.e. the 1st sample on fft spectrum
                                             % Note that end/2 means only
                                             % the first side from the
                                             % FFT mirror has been chosen

% % to cater for the case where a peak gives a peak composed to two points in
% % its derivative signal
% if  (abs(fft_matrix_for_max_location (1+max_ind+1,1)) >=  abs(fft_matrix_for_max_location (1+max_ind,1)) )% '1+'
%     max_ind = max_ind +1
%     shift_index = 1
% else
%     shift_index = 0
% end

if use_of_derivative_ON == 1
     max_ind = max_ind +1
    %shift_index = 1
else
    %shift_index = 0
end

% [ max_value2, max_ind2] = max (fft_matrix_for_max_location((end/2)+1:end)) % the other mirror 
%                
% % if  abs(fft_matrix_for_max_location (end/2+1+max_ind2-1,1)) >=  abs(fft_matrix_for_max_location (end/2+1+max_ind2,1)) % '-1' due to mirror effect 
% %     max_ind2 = max_ind2 +1
% % end

max_ind2 = N - max_ind +1;

%max_ind2 = max_ind2 + shift_index


dummy_fft = zeros(N,1); % as i wish to remove all but the MH so better use a zero filled matrix instead of removing all the signal around the MH
dummy_fft = dummy_fft + 1e-12;  % just some dust added to it to ve a dB spectrum


% % remove every thing except the MAX from furthur analysis
% dummy_fft(max_ind+1,1) = max_value;   % '+1' to cater for the DC index correction
% dummy_fft(end/2+max_ind2,1) = max_value2;  % add 'end/2' to correct the offset in max_ind2


% remove every thing except the MAX from furthur analysis
dummy_fft(max_ind+1,1) = fft_input(max_ind+1,1);   % '+1' to cater for the DC index correction
%dummy_fft(end/2+max_ind2,1) = fft_input(end/2+max_ind2,1);  % add 'end/2' to correct the offset in max_ind2
dummy_fft(max_ind2,1) = fft_input(max_ind2,1);


%% FFT spectrum plots
scale = 4*1;


dummy_fft_normal = (dummy_fft.*conj(dummy_fft))/N;
dummy_fft_normal_db = db(dummy_fft_normal,'power');

figure('name','double vvv  ')
subplot(1,1,1); plot(f(1:round(length(f)/(scale))),dummy_fft_normal_db(1:round(length(f)/(scale)))),grid on
legend('dummy\_fft\_normal_db')

fft_input_normal = (fft_input.*conj(fft_input))/N;
fft_input_normal_db = db(fft_input_normal,'power');


figure('name','double vvv  ')
subplot(1,1,1); plot(f(2:round(length(f)/(scale))),fft_input_normal_db(2:round(length(f)/(scale)))),grid on
legend('fft\_input_normal\_db')
xlabel('Frequency ( Hz )')
ylabel ('Power ( dB )')

fft_input_derivative_normal = (fft_input_derivative.*conj(fft_input_derivative))/N;
fft_input_derivative_normal_db = db(fft_input_derivative_normal,'power');


figure('name','double vvv  ')
subplot(1,1,1); plot(f(1:round(length(f)/(scale))),fft_input_derivative_normal_db(1:round(length(f)/(scale)))),grid on
legend('fft\_input\_derivative\_normal\_db')

%% IFFT
MH_signal = ifft(dummy_fft);  % MH  = main harmonic signal

figure('name','double vvv  ')
plot(MH_signal,'black')
hold on;
plot(input_signal,'red')
legend('main harmonic selecting filter output','input signal')