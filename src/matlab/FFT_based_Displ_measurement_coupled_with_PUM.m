% FFT_based_Displ_measurement_of_SM_signals


%% SM signal generation

clear all
close all
clc

global xc_estim N theta0


phi = pi/2;
C = 1.5;
alpha = 5;
amp = 10*0.5;   % No of Lambda   : 16 gets false detection
K = 5;


frequency = 20;
sampling_f = 2^16;  % X samples per sec
dt = 1/sampling_f;

%M=1e7;      % total no of acquisition points
time_in_sec = 1;
M = sampling_f*time_in_sec;     % total no of acquisition points

time = [0:dt:time_in_sec]';
time = time(1:end-1);

ramp_ON = 0
if ramp_ON == 1
    
    ramp = zeros(M,1);
    samples_per_period = (sampling_f)/frequency;
    samples_per_half_period = samples_per_period/2;
        
    ramp_pos = [-1:1/samples_per_half_period*2 :1]';
    ramp_neg = [1:-1/samples_per_half_period*2 :-1]';
    ramp_unit = [ ramp_pos(1:end-1) ; ramp_neg(1:end-1)];
    
%     figure('name','double vvv  ')
%     plot(ramp_unit)
    
    
    for i = 1:1:round(M/samples_per_period)
        ramp( (i-1)*samples_per_period + 1: i*samples_per_period,1) = ramp_unit;
    end
    
%     figure('name','double vvv  ')
%     plot(ramp)
    
    Displ = 2*pi*amp*ramp;  %
    
else
    
    
    Displ1 = 1*2*pi*(amp/1)*(sin(2*pi*((1/M)*frequency*(dt*M))*[0:M-1] + 0*pi/2) )';  %
    Displ2 = 0*2*pi*(amp/24)*(sin(2*pi*((1/M)*(frequency*5)*(dt*M))*[0:M-1] + 0*pi/2) )';  %
    Displ = Displ1 + Displ2;
end

Displ = Displ(1:2^16);


% xs_sim = K*pi - atan(5) + 2*pi*amp*(sin(2*pi*0.001*[0:9999]) )+ phi;
% Displ = 2*pi*amp*(sin(2*pi*0.001*[0:9999]) );


xs_sim = K*pi - atan(alpha) + Displ + phi;

N = length(xs_sim);


xc_sim = xc_fonction_xs_interpol_f_xs(xs_sim,C,atan(alpha));


P_sim = cos(xc_sim);

P_sim = P_sim(1:2^16);

figure('name','double vvv  ')
plot(P_sim(1:length(P_sim)/10))
legend('P_sim')


%% PUM

P = P_sim;


%% Tear a hole in the signal

%P(end/10: 1.5*end/10) = 0*P(end/10: 1.5*end/10);


% reprends 

%N = length(P);                          
seuil_pos = 0.554
seuil_neg = 0.554

%P = filtfilt(ones(15,1)/15,1,P);
%P_org = filtfilt(ones(15,1)/15,1,P_org);

seuil_pos = 0.54
seuil_neg = 0.54

chopped = 50;
%P_long = interp(P,20);
[trans,ac,P_norm] = static_trans_detection( P, seuil_pos,seuil_neg,chopped );

%trans(6810,1) = 1;


N = length(trans);

figure('name','p and trans')
plot((1:N),trans*max(P)*2,'r',(1:N),P,'b'),grid on
xlabel ('p  ')
legend('trans','P\_long')
ylabel('Amplitude')

figure('name','p and trans')
plot(time,P),grid on
xlabel ('Time (s)  ')
ylabel('Amplitude (a.u.)')

N = length(P);


staircase = filter(1,[1 -1],trans*pi*2);

under_sampling_ON = 0
if under_sampling_ON == 1
%P = downsample(P,5);
%time = downsample(time,5);
staircase = downsample(staircase,20);
%Displ = downsample(Displ,5);
end
N = length(P);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%P = filtfilt(ones(2)/2,1,P);         % Tr�s l�ger filtrage de P avec un filtre non causal
P = P - (max(P)+min(P))/2;           % Correction pour avoir P entre -M et +M
M_estim = max(abs(P));               % Amplitude max
P = P/M_estim;                       % On a maintenant    -1 < P < 1
P_norm = P;
ac = acos(P);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% original caro code after TRANS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%N =length(trans);
    theta0 = atan(5);    % Phase � l'origine du sinus de x_s = f(x_c)

xc_estim = (ac + staircase);      % xc estim� est obtenu par addition ou soustraction de 2*pi aux discontinuit�s
%xc_estim = (0 + filter(1,[1 -1],trans*pi*2)); 


xo=[2 2];                                 % initialisation de fminearch
C_alpha_opt = fminsearch(@mafonction_trouve_optimum_C_theta,xo);
C_opt = C_alpha_opt(1);
theta_corr_opt = C_alpha_opt(2);
theta_opt = theta_corr_opt +theta0;


xs_estim = xc_estim + C_opt*sin(xc_estim+theta_corr_opt+atan(5));

xs_estim_lisse = filtfilt(ones(50,1)/50,1,xs_estim);       % petit filtrage
%xs_estim_lisse = xs_estim  ; % No filtering

xs_estim_lisse = xc_estim;  % IF no [C theta] routine was used


xs_estim_lisse = xs_estim_lisse - mean(xs_estim_lisse);

xs_estim_lisse_org = xs_estim_lisse;

figure
plot(xs_estim_lisse), grid on

%% FFT based main harmonic selection of PUM signal

One_harmonic_detector_ON = 1;
if One_harmonic_detector_ON == 1
[xs_estim_lisse] = FFT_based_Main_Harmonic_Selector_Filter (xs_estim_lisse,sampling_f);

else
% Multiple harmonic selection
[xs_estim_lisse] = FFT_based_Main_and_Secondary_Harmonic_Selector_Filter (xs_estim_lisse,sampling_f);
end


xs_estim_lisse_org = (xs_estim_lisse_org - (max(xs_estim_lisse_org(N/5:N -N/5)) + min(xs_estim_lisse_org(N/5:N -N/5)))/2);
xs_estim_lisse_meaned = (xs_estim_lisse - (max(xs_estim_lisse(N/5:N -N/5)) + min(xs_estim_lisse(N/5:N -N/5)))/2);


%% The DISPLACEME?NT FROM THE PHASE
lambda = 0.785

xs_estim_lisse_org = (xs_estim_lisse_org ./(2*pi)).*(lambda/2);    %(0.785/2);
xs_estim_lisse_meaned = (xs_estim_lisse_meaned ./(2*pi)).*(lambda/2);    %(0.785/2);


%xc_estim = (xc_estim ./(2*pi)).*(lambda/2);
%xxx = (xxx ./(2*pi)).*(lambda/2);    %(0.785/2);

% INVERT PUM

xs_estim_lisse_meaned = -xs_estim_lisse_meaned;
xs_estim_lisse_org = -xs_estim_lisse_org;

Displ_in_nm =(Displ ./(2*pi)).*(lambda/2); 


xs_estim_lisse_meaned = xs_estim_lisse_meaned - mean(xs_estim_lisse_meaned);
Displ_in_nm  = Displ_in_nm - mean (Displ_in_nm);

err = xs_estim_lisse_meaned(100:end-100)-Displ_in_nm(100:end-100);
err_in_nm = 1000*err;


figure('name','p and trans')
subplot(1,1,1);plot(time(100:end-100),[xs_estim_lisse_meaned(100:end-100),xs_estim_lisse_org(100:end-100)])
xlabel('Time ( s )')
ylabel ('Amplitude ( um )')


figure('name','p and trans')
subplot(2,1,1);plot(time(100:end-100),[xs_estim_lisse_meaned(100:end-100),Displ_in_nm(100:end-100)])
xlabel('Time ( s )')
ylabel ('Amplitude ( um )')
legend('recov','org')
subplot(2,1,2);plot(time(100:end-100),[err_in_nm])
legend('Error in nm')
xlabel('Time ( s )')
ylabel ('Amplitude ( nm )')

rms_error_in_nm = sqrt(mean(err_in_nm.^2))


%% FFT
N = length(P_sim);

fft_P_sim = fft(P_sim);
fft_P_sim_normal = (fft_P_sim.*conj(fft_P_sim))/N;
fft_P_sim_normal_db = db(fft_P_sim_normal,'power');
fft_P_sim_theta = angle(fft_P_sim);

f=[0:1:N/2-1]*sampling_f/N;
f=f';
%%%  f=[0:1:N-1]*fs/N;%fk=k fs/N where k=0,1,2,...N-1

% figure('name','double vvv  ')
% subplot(4,1,1); plot(f,dsp_PP(1:(N/2))),grid on
% legend('Freq. Spectrum of Reconst. Displ')
% xlabel('Frequency ( Hz )')
% ylabel ('Power ( dB )')
% subplot(4,1,2:4); plot(f(1:round(length(f)/(16*8*8))),dsp_PP(1:round(length(f)/(16*8*8)))),grid on
% legend('Freq. Spectrum (zoom)')
% xlabel('Frequency ( Hz )')
% ylabel ('Power ( dB )')


scale = 8;
figure('name','double vvv  ')
subplot(1,1,1); plot(f(1:round(length(f)/(scale))),fft_P_sim_normal_db(1:round(length(f)/(scale)))),grid on
legend('Freq. Spectrum of Reconst. Displ')
xlabel('Frequency ( Hz )')
ylabel ('Power ( dB )')


scale = 1;
figure('name','double vvv  ')
subplot(1,1,1); semilogx(f(1:round(length(f)/(1))),fft_P_sim_normal_db(1:round(length(f)/(1)))),grid on
legend('Freq. Spectrum of Reconst. Displ')
xlabel('Frequency ( Hz )')
ylabel ('Power ( dB )')




% fft_P_sim = fft(P_sim);
% fft_P_sim_normal = (fft_P_sim.*conj(fft_P_sim))/N;
% fft_P_sim_normal_db = db(fft_P_sim_normal,'power');
% fft_P_sim_theta = angle(fft_P_sim);


%% FFT peaks location

[max_value, max_ind] = max(fft_P_sim_normal_db)  ;    % find max of spectrum

f_local_peak = f(max_ind)

First_max = max_value;

peaks = 50;
f_peak = zeros(peaks,5);    % it contains the info about the peaks in Spectrum
%f_peak_adj = f_peak ;

n = 0;

%fft_P_sim2 = fft_P_sim;

%while max_value > First_max - 20        % while the present max in the corrupted signal has atleast atleast 13 db
while n < 20        % find only the first N max peaks
    
    f_present = f(max_ind);
    
    
    %    if f_present >= 0.85 && f_present <= f(end)    % verify if the freq peak is in the operating band
    % if f_present > 1   && f_present <= f(end-1)
    
    n = n+1;
    
    %   if max_ind ~= 1
    
    f_peak(n,1) = f(max_ind,1);                         % peak freq
    f_peak(n,2) = fft_P_sim_normal_db(max_ind,1);
    f_peak(n,3) = fft_P_sim_theta(max_ind,1);
    
    
    fft_P_sim_normal_db(max_ind,1) = -100;       % remove this present peak from furthur analysis
    fft_P_sim_normal_db(length(fft_P_sim) - max_ind + 2 ,1) = -100;
    %   end
    
    %     else
    %         fft_P_sim_normal_db(max_ind,1) = -150;   % remove this present peak from furthur analysis
    %         fft_P_sim_normal_db(length(fft_P_sim) - max_ind + 2 ,1) = -150;
    %
    %
    %     end
    
    [max_value, max_ind] = max(fft_P_sim_normal_db(1:(N/2)-1,1));  % find the next most important peak in the remaining spectrum and its value
    f_local_peak = f(max_ind)
    
end


