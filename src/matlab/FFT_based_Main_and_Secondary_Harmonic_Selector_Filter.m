%% this function has time domain input and output, u give it the input
%% matrix  and its sampling_f and then the code returns just a signal based on the main harmonic of the input signal

%function [MH_signal] = FFT_based_Main_and_Secondary_Harmonic_Selector_Filter (input_signal,sampling_f)
function [f,dummy_fft_normal_db,fft_input_normal_db,MH_signal] = FFT_based_Main_and_Secondary_Harmonic_Selector_Filter (input_signal,sampling_f)


N = length(input_signal);

%dt = time(2,1) - time(1,1);
%sampling_f = 1/dt;
f=[0:1:N/2-1]*sampling_f/N;
f=f';



fft_input = fft(input_signal);

% Derivative of FFT 's abs values
fft_input_derivative = zeros(length(fft_input),1);
fft_input_abs = abs(fft_input);
fft_input_derivative(1:end-1) = fft_input_abs(2:end) - fft_input_abs(1:end-1); % note the flipped derivative to cater for the decaying shape of FFT spectrum

use_of_derivative_ON =1
if use_of_derivative_ON == 1
    fft_matrix_for_max_location = fft_input_derivative;
else
    fft_matrix_for_max_location = fft_input;
end


dummy_fft = zeros(N,1); % as i wish to remove all but the MH so better use a zero filled matrix instead of removing all the signal around the MH
dummy_fft = dummy_fft + 1e-12;  % just some dust added to it to ve a dB spectrum



fft_input_derivative_normal = (fft_input_derivative.*conj(fft_input_derivative))/N;
fft_input_derivative_normal_db = db(fft_input_derivative_normal,'power');                                                                    
    

fft_input_normal = (fft_input.*conj(fft_input))/N;
fft_input_normal_db = db(fft_input_normal,'power');

fft_matrix_for_max_location_normal = (fft_matrix_for_max_location.*conj(fft_matrix_for_max_location))/N;
fft_matrix_for_max_location_normal_db = db(fft_matrix_for_max_location_normal,'power');


[max_value ,max_ind] = max(fft_matrix_for_max_location(2:end/2,1))  % Select max from everything except the DC value  i.e. the 1st sample on fft spectrum
                                                                    % Note that end/2 means only
                                                                    % the first side from the
                                                                    % FFT mirror has been chosen

                                                                    
initial_first_max = fft_matrix_for_max_location_normal_db(max_ind+1,1);
mean_db = mean(fft_matrix_for_max_location_normal_db)

% loop = 0;
% while loop<=4
%         
%     loop = loop +1;
    
%while fft_matrix_for_max_location_normal_db(max_ind+1,1) > mean_db + 100
%while fft_matrix_for_max_location_normal_db(max_ind+1,1) > initial_first_max -31  % while the current max is atleast 30dB below the initial max
while fft_matrix_for_max_location_normal_db(max_ind+1,1) > 50  % while the current max is atleast 30dB 
    
    if use_of_derivative_ON == 1
        
        max_ind = max_ind +1
        max_ind2 = N - max_ind +1;
        % remove this current max from further analysis
        fft_matrix_for_max_location(max_ind:max_ind+1,1) = 0;  % as the derivative signal has double peak for each single peak in simple fft
        fft_matrix_for_max_location(max_ind2-1:max_ind2,1,1) = 0; % as the derivative signal has double peak for each single peak in simple fft
        
        %shift_index = 1
    else
        max_ind2 = N - max_ind +1;    
        %shift_index = 0
        fft_matrix_for_max_location(max_ind+1,1) = 0;  %  single peak in simple fft
        fft_matrix_for_max_location(max_ind2,1,1) = 0; % single peak in simple fft

    end
    

    
    
    
    % remove every thing except the MAX from furthur analysis
    dummy_fft(max_ind+1,1) = fft_input(max_ind+1,1);   % '+1' to cater for the DC index correction
    %dummy_fft(end/2+max_ind2,1) = fft_input(end/2+max_ind2,1);  % add 'end/2' to correct the offset in max_ind2
    dummy_fft(max_ind2,1) = fft_input(max_ind2,1);
    

 
    % find the next max
    [max_value ,max_ind] = max(fft_matrix_for_max_location(2:end/2,1))  % Select max from everything except the DC value  i.e. the 1st sample on fft spectrum

    

end

%% FFT spectrum plots
scale = 1*4*1;


dummy_fft_normal = (dummy_fft.*conj(dummy_fft))/N;
dummy_fft_normal_db = db(dummy_fft_normal,'power');

figure('name','double vvv  ')
subplot(1,1,1); plot(f(1:round(length(f)/(scale))),dummy_fft_normal_db(1:round(length(f)/(scale)))),grid on
legend('dummy\_fft\_normal\_db')
xlabel('Frequency ( Hz )')
ylabel ('Power ( dB )')


figure('name','double vvv  ')
subplot(1,1,1); plot(f(2:round(length(f)/(scale))),fft_input_normal_db(2:round(length(f)/(scale)))),grid on
legend('fft_input_normal_db')
xlabel('Frequency ( Hz )')
ylabel ('Power ( dB )')



% figure('name','double vvv  ')
% subplot(1,1,1); plot(f(1:round(length(f)/(scale))),fft_input_derivative_normal_db(1:round(length(f)/(scale)))),grid on
% legend('fft_input_derivative_normal_db')

%% IFFT
MH_signal = ifft(dummy_fft);  % MH  = main harmonic signal

figure('name','double vvv  ')
plot(MH_signal,'black')
hold on;
plot(input_signal,'red')
legend('main harmonic selecting filter output','input signal')