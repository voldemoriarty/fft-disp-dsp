fs = importdata('../test/data/sampling_f.dat');
psim = importdata('../test/data/psim.dat');
disp = importdata('../test/data/disp.dat');

lambda = 0.785;

psim_norm = 2*(psim - min(psim))/(max(psim) - min(psim)) - 1;   % normalize [-1 1]

trans = psim_norm(2:end) - psim_norm(1:end-1);  % compute first forward difference

trans = [trans; trans(end)];                    % equalize the sizes

trans_max = 0.54*max(trans);                    % get the threshold

trans(abs(trans) < trans_max) = 0;              % get the transitions
trans = trans ./ (abs(trans) + 1e-50);          % add a very small value to avoid NaNs

staircase = cumsum(trans * 2 * pi);             % each trans corresponds to 2*pi phase

phase = -staircase - acos(psim_norm);           % add acos to get phase unwrapped

phase_f = fft(phase);
dc = phase_f(1);
phase_f(1) = 0;                                             % remove dc, offset
phase_mag_eq = abs(real(phase_f)) + abs(imag(phase_f));     % gate saving jugaar; saves 2 multipliers
phase_mag = abs(phase_f);


[phase_f_max, i] = max(phase_mag_eq);
phase_f(phase_mag_eq < phase_f_max) = 0;

% rec = ifft(phase_f, 'symmetric');
% another gate saving jugaar; if we have only one frequency to extract
% we dont need to do entire ifft, we only need one sine wave with a phase
% we can do that with only one addition and two multiplications

% for one pt/hz, i is the frequency we need
% but we still need the computation for phase

Xmx1 = phase_f(i);          % symmetric pair
Xmx2 = conj(Xmx1);          % can get away with a conjugate though

k = 0:fs-1;

t = exp(pi*2j*(i-1)*k / fs);
rec = 1/fs * (Xmx1 * t + Xmx2 * conj(t));
rec = real(rec');


Displ_in_nm = (rec ./(2*pi)).*(lambda/2) * 1000;
disp_in_nm = (disp ./(2*pi)).*(lambda/2) * 1000;

subplot(2, 1, 1);
plot(0:1/fs:(1-1/fs), Displ_in_nm);
hold on;
title('Displacement (nm)');
plot(0:1/fs:(1-1/fs), disp_in_nm);
legend('Recovered', 'Original');

subplot(2, 1, 2);
plot(0:1/fs:(1-1/fs), disp_in_nm - Displ_in_nm);
title('Rec - Orig (nm)');
