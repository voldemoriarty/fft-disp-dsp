function [ psim, disp ] = gen_psim(fs, t, f, lambda)
    phi = pi/2;
    C = 1.5;
    alpha = 5;
    amp = 10*0.5;                   % No of Lambda   : 16 gets false detection
    K = 5;

    %fs = 2^16;              % X samples per sec
    dt = 1/fs;

    time_in_sec = t;
    M = fs*time_in_sec;     % total no of acquisition points

    time = (0:dt:time_in_sec)';
    time = time(1:end-1);

    disp = zeros(M, 1);

    freq_band = f;

    for i = freq_band
        disp = disp + sin(2*pi*i*time);
    end

    disp = disp / length(freq_band) * 2*pi*amp;

    xs_sim = K*pi - atan(alpha) + disp + phi;

    psim = cos(xc_fonction_xs_interpol_f_xs(xs_sim, C, atan(alpha)));
    
    % convert disp to nm
    disp = disp / (2*pi) * lambda / 2;
end

