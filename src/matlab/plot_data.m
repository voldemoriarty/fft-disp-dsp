disp = importdata('../test/data/disp.dat');
rec = importdata('../test/data/ifft-real.dat');
ph = importdata('../test/data/phase.dat');
sampling_f = importdata('../test/data/sampling_f.dat');

cfft = importdata('../test/data/fft-real.dat') + 1j * importdata('../test/data/fft-imag.dat');
cfftt = importdata('../test/data/fft-thres-real.dat') + 1j * importdata('../test/data/fft-thres-imag.dat');
plot(ph);
title('Phase Unwrap');
figure;

subplot(3,1,1);
plot(rec), grid on;
title('Recovered in C');

subplot(3,1,2);
plot(disp(1:length(rec))), grid on;
title('Original Disp');

subplot(3,1,3);
plot(rec - disp(1:length(rec))), grid on;
title('Difference');

%% The DISPLACEMENT FROM THE PHASE
dt = 1/sampling_f;

%M=1e7;      % total no of acquisition points
time_in_sec = 1;
M = sampling_f*time_in_sec;     % total no of acquisition points

time = [0:dt:time_in_sec]';
time = time(1:end-1);

lambda = 0.785

xs_estim_lisse_org = ph;
xs_estim_lisse = rec;

N = length(ph);
Displ = disp;

xs_estim_lisse_org = (xs_estim_lisse_org - (max(xs_estim_lisse_org(N/5:N -N/5)) + min(xs_estim_lisse_org(N/5:N -N/5)))/2);
xs_estim_lisse_meaned = (xs_estim_lisse - (max(xs_estim_lisse(N/5:N -N/5)) + min(xs_estim_lisse(N/5:N -N/5)))/2);

xs_estim_lisse_org = (xs_estim_lisse_org ./(2*pi)).*(lambda/2);    %(0.785/2);
xs_estim_lisse_meaned = (xs_estim_lisse_meaned ./(2*pi)).*(lambda/2);    %(0.785/2);


%xc_estim = (xc_estim ./(2*pi)).*(lambda/2);
%xxx = (xxx ./(2*pi)).*(lambda/2);    %(0.785/2);

Displ_in_nm =(Displ ./(2*pi)).*(lambda/2); 


xs_estim_lisse_meaned = xs_estim_lisse_meaned - mean(xs_estim_lisse_meaned);
Displ_in_nm  = Displ_in_nm - mean (Displ_in_nm);

err = xs_estim_lisse_meaned(100:end-100)-Displ_in_nm(100:end-100);
err_in_nm = 1000*err;


figure('name','p and trans')
subplot(1,1,1);plot(time(100:end-100),[xs_estim_lisse_meaned(100:end-100),xs_estim_lisse_org(100:end-100)])
xlabel('Time ( s )')
ylabel ('Amplitude ( um )')


figure('name','p and trans')
subplot(2,1,1);plot(time(100:end-100),[xs_estim_lisse_meaned(100:end-100),Displ_in_nm(100:end-100)])
xlabel('Time ( s )')
ylabel ('Amplitude ( um )')
legend('recov','org')
subplot(2,1,2);plot(time(100:end-100),[err_in_nm])
legend('Error in nm')
xlabel('Time ( s )')
ylabel ('Amplitude ( nm )')

rms_error_in_nm = sqrt(mean(err_in_nm.^2))
