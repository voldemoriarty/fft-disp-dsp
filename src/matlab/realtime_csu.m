% this works on streaming input data
% suitable for realtime implementations

function out = realtime_csu(psim) 
    N = length(psim);

    out = zeros(size(psim));

    prev = 0;
    curr = 0;
    maxc = 0.5;
    minc = -0.5;
    maxd = 0.5;
    mind = -.5;
    stair = 0;

    for i = 1:N
        prev = curr;
        curr = psim(i);

        if curr > maxc
            maxc = curr;
        elseif curr < minc
            minc = curr;
        end
        
        curr = 2 * (curr - minc) / (maxc - minc) - 1;

        del = curr - prev;

        if del > maxd
            maxd = del;
        elseif del < mind
            mind = del;
        end

        if del > 0.54 * maxd
            stair = stair + 2*pi;
        elseif del < 0.54 * mind
            stair = stair - 2*pi;
        end

        out(i) = -stair + 2*pi*(curr + 1) / 2;
    end

end