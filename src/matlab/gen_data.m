% This file contains code to generate the SM signal and save it to disk

% FFT_based_Displ_measurement_of_SM_signals


% SM signal generation

clc

global N 


phi = pi/2;
C = 1.5;
alpha = 5;
amp = 10*0.5;                   % No of Lambda   : 16 gets false detection
K = 5;

frequency = 20;
sampling_f = 2^16;              % X samples per sec
dt = 1/sampling_f;

time_in_sec = 1;
M = sampling_f*time_in_sec;     % total no of acquisition points

time = (0:dt:time_in_sec)';
time = time(1:end-1);


ramp_ON = 0;
if ramp_ON == 1
    
    ramp = zeros(M,1);
    samples_per_period = (sampling_f)/frequency;
    samples_per_half_period = samples_per_period/2;
        
    ramp_pos = (-1:1/samples_per_half_period*2 :1)';
    ramp_neg = (1:-1/samples_per_half_period*2 :-1)';
    ramp_unit = [ ramp_pos(1:end-1) ; ramp_neg(1:end-1)];

    for i = 1:1:round(M/samples_per_period)
        ramp( (i-1)*samples_per_period + 1: i*samples_per_period,1) = ramp_unit;
    end
    
    Displ = 2*pi*amp*ramp;  %
else
    Displ1 = 1*2*pi*(amp/1)*(sin(2*pi*((1/M)*frequency*(dt*M))*(0:M-1) + 0*pi/2) )';
    Displ2 = 0*2*pi*(amp/24)*(sin(2*pi*((1/M)*(frequency*5)*(dt*M))*(0:M-1) + 0*pi/2) )';
    Displ = Displ1 + Displ2;
end



% xs_sim = K*pi - atan(5) + 2*pi*amp*(sin(2*pi*0.001*[0:9999]) )+ phi;
% Displ = 2*pi*amp*(sin(2*pi*0.001*[0:9999]) );


xs_sim = K*pi - atan(alpha) + Displ + phi;

N = length(xs_sim);

xc_sim = xc_fonction_xs_interpol_f_xs(xs_sim,C,atan(alpha));


P_sim = cos(xc_sim);

file = fopen('../test/data/disp.dat', 'w');             % write original displacement
fprintf(file, '%f\n', single(Displ));
fclose(file);

file = fopen('../test/data/psim.dat', 'w');             % write power signal
fprintf(file, '%f\n', single(P_sim));
fclose(file);

file = fopen('../test/data/frequency.dat', 'w');        % write frequency
fprintf(file, '%f\n', single(frequency));
fclose(file);

file = fopen('../test/data/amplitude.dat', 'w');        % write amplitude
fprintf(file, '%f\n', single(amp));
fclose(file);

file = fopen('../test/data/sampling_f.dat', 'w');       % write sampling frequency
fprintf(file, '%f\n', single(sampling_f));
fclose(file);
