f = 1;
t = 2;
fs = 1e3;
time = 0:1/fs:t-1/fs;

[psim, disp] = gen_psim(fs, t, f);
unwrap = realtime_phase_unwrap(psim);

subplot(3, 1, 1)
plot(time, disp)
xlabel('Time (s)')
ylabel('Displacement ({\mu}m)')
subplot(3, 1, 2)
plot(time, psim)
xlabel('Time (s)')
ylabel('Normalized PSIM')
subplot(3, 1, 3)
plot(time, unwrap)
xlabel('Time (s)')
ylabel('Displacement ({\mu}m)')