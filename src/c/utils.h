// common functions that are used
#include <math.h>
#include <stdio.h>

#ifndef PI
#define PI 3.14159265358979323846264338327950288f
#endif

typedef float real;

typedef struct cplx {
	real Re, Im;
} cplx;


#ifdef __cplusplus 
extern "C" {
#endif
    
real max(const real *arr, const int n);
real min(const real *arr, const int n);
void min_max(const real* arr, const int n, real* max, real* min);
void min_max_loc(const real* arr, const int n, real* max, real* min, int* minl, int* maxl);

void gain(real *arr, const real val, const int n);
void offset(real *arr, const int n, const real off);

void gain2(real *dst, real *src, const int n, real mult);
void offset2(real *dst, real *src, const int n, real mult);

void normalize01(real *arr, const int n);
void normalize11(real *norm01, const int n);

real cosinv(real val);
real sine(real val);
real cosine(real val);

void cosinv2(real *val, const int n);
void sine2(real *val, const int n);
void cosine2(real *val, const int n);

void magnitude(real* x, real* y, const int n);
void magnitude2(real *abs, real *x, real *y, const int n);

void print_vector_cplx(const char *title, cplx *x, const int n);
void print_vector(const char *title, const real *x, const real *y, const int n);

void fft(real *vx, real *vy, const int n, real *tmpx, real *tmpy);
void _ifft(real *vx, real *vy, const int n, real *tmpx, real *tmpy);
void ifft(real *vx, real *vy, const int n, real *tmpx, real *tmpy);

void threshold(real* xrr, real* yrr, real* temp, const int n);

real mean(const real *dat, const int n);
void zero_out(real *dat, const int n);

int transition_detection(const real *arr, int* tlocs, int *tvals, const int n, const real threshold);
void phase_unwrap(real* smsig, real* Phase, real* staircase_pi, const int n);

#ifdef __cplusplus
}
#endif
