#include "utils.h"

void phase_unwrap(
    real* smsig,        // self mixing signal 
    real* Phase,        // output rough unwrapped phase, must be init to zero
    real* staircase_pi,
    const int n) {
    
    real running_phase = 0.0f, mx, mn, avg;
    const real pthresh = 0.54f, nthresh = 0.54f;
    
//    min_max(smsig, n, &mx, &mn);
//    avg = -(mx + mn) / 2;
//    offset(smsig, n, avg);
    
    normalize01(smsig, n);
    normalize11(smsig, n);
   
    Phase[0] = cosinv(smsig[0]);
    staircase_pi[0] = 0.0f;
    
    for (int i = 1; i < n; ++i) {
        staircase_pi[i] = smsig[i] - smsig[i - 1];    // calculate back differences
        Phase[i] = cosinv(smsig[i]);
    }
    
    
    min_max(staircase_pi, n, &mx, &mn);
    
//    zero_out(staircase_pi, 50);                       // chop the signal as seen in matlab
//    zero_out(staircase_pi + n - 50, 50);

     for (int i = 0; i < n; ++i) {
         real tval = staircase_pi[i];
         staircase_pi[i] = running_phase;

         if (tval > mx * pthresh) {
             running_phase += 2 * PI;
         } else if (tval < mn * nthresh) {
             running_phase -= 2 * PI;
         }
     }

     staircase_pi[n-1] = running_phase;

     for (int i = 0; i < n; ++i) {
         Phase[i] += staircase_pi[i];
         Phase[i] *= -1;
     }
}
