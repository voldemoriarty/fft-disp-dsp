#include "utils.h"


void fft(real *vx, real *vy, const int n, real *tmpx, real *tmpy) {
    if (n > 1) {
        int k, m;    cplx z, w; real *vox, *voy, *vex, *vey;
        vex = tmpx;
        vey = tmpy; 
        vox = tmpx + n / 2;
        voy = tmpy + n / 2;
        for (k = 0; k < n / 2; k++) {
            vex[k] = vx[2 * k];
            vey[k] = vy[2 * k];
            vox[k] = vx[2 * k + 1];
            voy[k] = vy[2 * k + 1];
        }
        fft(vex, vey, n / 2, vx, vy);
        fft(vox, voy, n / 2, vx, vy);
        for (m = 0; m < n / 2; m++) {
            w.Re = cosine(2 * PI*m / (real)n);
            w.Im = -sine(2 * PI*m / (real)n);
            z.Re = w.Re*vox[m] - w.Im*voy[m];
            z.Im = w.Re*voy[m] + w.Im*vox[m];
            vx[m] = vex[m] + z.Re;
            vy[m] = vey[m] + z.Im;
            vx[m + n / 2] = vex[m] - z.Re;
            vy[m + n / 2] = vey[m] - z.Im;
        }
    }
}

void _ifft(real *vx, real *vy, const int n, real *tmpx, real *tmpy) {
    if (n > 1) {
        int k, m;    cplx z, w; real *vox, *voy, *vex, *vey;
        vex = tmpx;
        vey = tmpy; 
        vox = tmpx + n / 2;
        voy = tmpy + n / 2;
        for (k = 0; k < n / 2; k++) {
            vex[k] = vx[2 * k];
            vey[k] = vy[2 * k];
            vox[k] = vx[2 * k + 1];
            voy[k] = vy[2 * k + 1];
        }
        _ifft(vex, vey, n / 2, vx, vy);
        _ifft(vox, voy, n / 2, vx, vy);
        for (m = 0; m < n / 2; m++) {
            w.Re = cosine(2 * PI*m / (real)n);
            w.Im = sine(2 * PI*m / (real)n);
            z.Re = w.Re*vox[m] - w.Im*voy[m];
            z.Im = w.Re*voy[m] + w.Im*vox[m];
            vx[m] = vex[m] + z.Re;
            vy[m] = vey[m] + z.Im;
            vx[m + n / 2] = vex[m] - z.Re;
            vy[m + n / 2] = vey[m] - z.Im;
        }
    }
}

void ifft(real *vx, real *vy, const int n, real *tmpx, real *tmpy) {
    _ifft(vx, vy, n, tmpx, tmpy);

    for (int i = 0; i < n; ++i) {
        vy[i] /= n;
        vx[i] /= n;
    }
} 
