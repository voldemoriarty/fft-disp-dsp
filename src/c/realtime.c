float prev = 0.0f, curr = 0.0f;
float minc = -1.0f, maxc = 1.0f, delinv = 0.0f;
float mind = -1.0f, maxd = 1.0f;
float stair = 0.0f;
short idx = 0;
const float scale = 11585.0f;
const float scale_inv = 1 / scale;

typedef struct {
    float re, im;
} cplx_t;

#define N 256

cplx_t buff0[N], buff1[N], buff2[N];
cplx_t *ibuff, *obuff, *pbuff;

void adc_interrupt() {
    output_left_sample((short)(obuff[idx].re * scale));

    // 1. convert sample to float
    prev = curr;
    curr = (float) input_left_sample() * scale_inv;

    // 2. update min, max
    if (curr > maxc) {
        maxc = curr;
        delinv = 1.0f / (maxc - minc);
    } else if (curr < minc) {   
        minc = curr;
        delinv = 1.0f / (maxc - minc);
    }

    // 3. normalize sample
    curr = 2.0f * (curr - minc) * delinv - 1.0f;
    // 4. compute delta
    const float del = curr - prev;
    // 5. update delmin, delmax
    if (del > maxd) {
        maxd = del;
    } else if (del < mind) {
        mind = del;
    }
    // 6. update staircase
    if (del > 0.54f * maxd) {
        stair += 2.0f * PI;
    } else if (del < 0.54f * mind) {
        stair -= 2.0f * PI;
    }
    // 7. unwrap phase
    ibuff[idx].re = -stair - acosf(curr);
    ibuff[idx].im = 0.0f;
}

float stairs = 0.0f;

void process() {
    // rotate the buff pointers
    // pbuff <= ibuff
    // obuff <= pbuff
    // ibuff <= obuff
    cplx_t *tmp = ibuff;
    ibuff = obuff;
    obuff = pbuff;
    pbuff = tmp;
    
    // now we have unwrapped phase
    // we just need to take it's fft
    cfft2_dit((float*)pbuff, (float*)W, N);

    // now we find the max freq
    // we use the sum(abs) method
    // only need to check N/2 vals
    float magmax = 0;
    pbuff[0].re = 0.0f;
    pbuff[0].im = 0.0f;
    for (short i = 1; i < N; ++i) {
        float mag = _fabs(pbuff[i].re) + _fabs(pbuff[i].im);
        obuff[i].im = mag;          // imag part of obuff isn't used, only real part is output
        if (mag > magmax) {
            magmax = mag;
        }
    }

    // now we threshold
    for (short i = 1; i < N; ++i) {
        float mag = obuff[i].im;    // use pre calculated mag
        if (mag < 0.01 * magmax) {
            pbuff[i].re = 0.0f;
            pbuff[i].im = 0.0f;
        }
    }

    // now do ifft
    icfft2_dit((float*)pbuff, (float*)W, N);

    // rotate pointers
    tmp = obuff;
    obuff = pbuff;
    pbuff = tmp;
    return;
}