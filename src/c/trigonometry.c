/*
 * File contains definition for trigonometric functions like sin cos
 * Reason for different implementation is that we can easily change from 
 * standard lib to lookup table approach
 */

#include "utils.h"

real cosine(real val) {
    return cosf(val);
}

void cosine2(real* val, const int n) {
    for (int i = 0; i < n; ++i) {
        val[i] = cosine(val[i]);
    }
}

real sine(real val) {
    return sinf(val);
}

void sine2(real* val, const int n) {
    for (int i = 0; i < n; ++i) {
        val[i] = sine(val[i]);
    }
}

real cosinv(real val) {
    return acosf(val);
}

void cosinv2(real* val, const int n) {
    for (int i = 0; i < n; ++i) {
        val[i] = cosinv(val[i]);
    }
}
