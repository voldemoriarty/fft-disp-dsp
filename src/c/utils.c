#include "utils.h"



void print_vector_cplx(const char *title, cplx *x, const int n)
{
    int i;
    printf("%s (dim=%d):", title, n);
    for (i = 0; i < n; i++)
        printf(" %5.2f+%5.2fi ", x[i].Re, x[i].Im);
    putchar('\n');
}


void print_vector(const char *title, const real *x, const real *y, const int n) {
    int i;
    printf("%s (dim=%d):\n", title, n);
    for (i = 0; i < n; ++i) {
        printf("%f + %fi\n", x[i], y[i]);
    }
}


void threshold(real* xrr, real* yrr, real* temp, const int n) {
    real mx = -INFINITY;

    for (int i = 0; i < n; ++i) {
        real mag_sq = xrr[i] * xrr[i] + yrr[i] * yrr[i];
        mx = mag_sq > mx ? mag_sq : mx;
        temp[i] = mag_sq;
    }

    for (int i = 0; i < n; ++i) {
        if (temp[i] < mx * 0.8f) {
            xrr[i] = 0;
            yrr[i] = 0;
        }
    }
}
void gain(real *arr, const real val, const int n) {
    for (int i = 0; i < n; ++i) {
        arr[i] *= val;
    }
}
void min_max(const real* arr, const int n, real* max, real* min) {
    *max = -INFINITY;
    *min = INFINITY;
    for (int i = 0; i < n; ++i) {
        *max = *max < arr[i] ? arr[i] : *max;
        *min = *min > arr[i] ? arr[i] : *min;
    }
}
void min_max_loc(const real* arr, const int n, real* max, real* min, int* minl, int* maxl) {
    *max = -INFINITY;
    *min = INFINITY;
    for (int i = 0; i < n; ++i) {
        *max =  *max < arr[i] ? arr[i] : *max;
        *maxl = *max < arr[i] ? i : *maxl;
        *min =  *min > arr[i] ? arr[i] : *min;
        *minl = *min > arr[i] ? i : *minl;
    }
}
real min(const real *arr, const int n) {
    real m = INFINITY;
    for (int i = 0; i < n; ++i) {
        m = arr[i] < m ? arr[i] : m;
    }
    return m;
}
real max(const real *arr, const int n) {
    real m = -INFINITY;
    for (int i = 0; i < n; ++i) {
        m = arr[i] > m ? arr[i] : m;
    }
    return m;
}

void normalize01(real *arr, const int n) {
    real max, min;
    min_max(arr, n, &max, &min);
    const real del = max - min;

    for (int i = 0; i < n; ++i) {
        arr[i] = (arr[i] - min) / del;
    }
}

void normalize11(real *norm01, const int n) {
    for (int i = 0; i < n; ++i) {
        norm01[i] = 2.0f*norm01[i] - 1.0f;
    }
}

void magnitude(real *x, real *y, const int n) {
    for (int i = 0; i < n; ++i) {
        x[i] = sqrtf(x[i]*x[i] + y[i]*y[i]);
    }
}

// returns number of transitions
// tlocs: locations of transitions
// tvals: direction of transitions

int transition_detection(const real *arr, int* tlocs, int *tvals, const int n, const real threshold) {
    int j = 0;
    for (int i = 0, j = 0; i < n - 1; ++i) {
        const real del = arr[i + 1] - arr[i];
        if (del > threshold) {
            tlocs[j] = i + 1;
            tvals[j] = -1;
            ++j;
            i += 4*4;                     // find transitions in a band of 16
        } else if (del < -threshold) {
            tlocs[j] = i + 1;
            tvals[j] = +1;
            ++j;
            i += 4*4;
        }
    }
    return j;
}

real mean(const real *dat, const int n) {
    real s = 0;
    for (int i = 0; i < n; ++i) {
        s += dat[i];
    }
    s /= n;
    return s;
}

void offset(real *dat, const int n, const real val) {
    for (int i = 0; i < n; ++i) {
        dat[i] += val;
    }
}

void zero_out(real *dat, const int n) {
    for (int i = 0; i < n; ++i) {
        dat[i] = 0;
    }
}