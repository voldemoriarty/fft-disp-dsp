----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:12:23 04/23/2016 
-- Design Name: 
-- Module Name:    csu - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_signed.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity csu is
    Port ( din : in  signed (31 downto 0);
           clk : in  STD_LOGIC;
           dout : out  signed (33 downto 0));
end csu;

architecture Behavioral of csu is
type ram_t is array (0 to 3) of signed(31 downto 0);
signal a : ram_t := (others => (others => '0'));
signal trans1:signed(31 downto 0):=(others => '0');
signal trans:signed(1 downto 0):=(others => '0');
signal st,std:signed(31 downto 0):=(others => '0');


begin




process(clk)
begin

if(clk='1' and clk'event) then
a(0)<=din;
a(1)<=a(0);
a(2)<=a(1);
a(3)<=a(2);
trans1<=a(1)-a(0);

	if (trans1 < -243497802 ) then
				trans<="11";
	elsif(trans1 > 243497802 ) then
				trans<="01";
	else
				trans<="00";
	end if;
	
--std <= st;

st <= (st + trans);
	
	
dout<=resize((signed(a(3)) + (st*536870912)),34) ;

	
end if;

end process;

end Behavioral;




